Source: watchman
Maintainer: Victor Westerhuis <victor@westerhu.is>
Section: utils
Priority: optional
Build-Depends: cargo:native,
               cmake,
               debhelper-compat (=13),
               dh-sequence-python3 <!nopython>,
               libboost-context-dev (>= 1.54.0),
               libboost-filesystem-dev (>= 1.51.0),
               libboost-program-options-dev (>= 1.51.0),
               libboost-regex-dev (>= 1.51.0),
               libboost-system-dev (>= 1.51.0),
               libboost-thread-dev (>= 1.54.0),
               libdouble-conversion-dev,
               libevent-dev,
               libfmt-dev,
               libgflags-dev,
               libgmock-dev,
               libgoogle-glog-dev,
               libgtest-dev,
               libpcre2-dev,
               libpython3-all-dev <!nopython>,
               librust-ahash-0.8+default-dev,
               librust-anyhow-1+default-dev,
               librust-atty-0.2+default-dev,
               librust-byteorder-1+default-dev,
               librust-bytes-1+default-dev,
               librust-bytes-1+serde-dev,
               librust-crossbeam-0.8+default-dev,
               librust-duct-0.13+default-dev,
               librust-futures-0.3+async-await-dev,
               librust-futures-0.3+default-dev,
               librust-maplit-1+default-dev,
               librust-nix-0.26+default-dev,
               librust-rayon-1+default-dev,
               librust-serde-1+default-dev,
               librust-serde-1+derive-dev,
               librust-serde-1+rc-dev,
               librust-serde-bytes-0.11+default-dev,
               librust-serde-json-1+default-dev,
               librust-serde-json-1+float-roundtrip-dev,
               librust-serde-json-1+unbounded-depth-dev,
               librust-structopt-0.3+default-dev,
               librust-sysinfo-0.28+default-dev,
               librust-thiserror-1+default-dev,
               librust-tokio-1+default-dev,
               librust-tokio-1+full-dev,
               librust-tokio-1+test-util-dev,
               librust-tokio-1+tracing-dev,
               librust-tokio-util-0.7+default-dev,
               librust-tokio-util-0.7+full-dev,
               librust-unicode-width-0.1+default-dev,
               libssl-dev,
               libstd-rust-dev,
               mercurial <!nocheck>,
               ninja-build,
               procps <!nocheck>,
               pybuild-plugin-pyproject <!nopython>,
               python3-all-dev:native <!nopython>,
               python3-setuptools <!nopython>,
               python3:any,
               rustc:native,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/watchman
Vcs-Git: https://salsa.debian.org/debian/watchman.git
Homepage: https://facebook.github.io/watchman
Rules-Requires-Root: no

Package: watchman
Architecture: linux-any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Suggests: python3-pywatchman,
Description: File watching service
 Watchman can be used to watch files and record when they actually change.
 It can be used to trigger actions (such as rebuilding assets) when
 matching files change. If you require to perform an action based on
 whether a file changes or not, watchman may be the tool you need. By
 giving watchman a pattern and an action to take when the files change,
 you can trigger an activity to be taken.

Package: python3-pywatchman
Architecture: linux-any
Section: python
Depends: watchman,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Description: Python library for Watchman and related utilities
 A Python library for watchman (the file watching service), and related
 utilities such as watchman-make and watchman-wait. Watchman can be used
 to watch files and record when they actually change. It can be used
 to trigger actions (such as rebuilding assets) when matching files
 change. If you require to perform an action based on whether a file
 changes or not, watchman may be the tool you need. By giving watchman
 a pattern and an action to take when the files change, you can trigger
 an activity to be taken.
Build-Profiles: <!nopython>
